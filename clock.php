<?php

declare(strict_types=1);

class Clock
{
    private $minutes;

    public function __construct($hour, $minutes = 0)
    {
        $totalMinutes = $this->calculateTotalMinutes($hour, $minutes);

        $minutesWithoutFullDays = $this->ignoreWholeDays($totalMinutes);

        $positiveTimeInMinutes = $this->ensurePositiveMinutes($minutesWithoutFullDays);

        $this->minutes = $positiveTimeInMinutes;
    }

    public function add($minutes): \Clock
    {
        return new Clock(0, $this->minutes + $minutes);
    }

    public function sub($minutes): \Clock
    {
        return $this->add(-$minutes);
    }

    public function __toString()
    {
        return sprintf('%02d:%02d', $this->minutes / 60, $this->minutes % 60);
    }

    private function calculateTotalMinutes($hour, $minutes): int
    {
        return ($hour * 60) + $minutes;
    }

    private function ignoreWholeDays($minutes): int
    {
        return $minutes % (24 * 60);
    }

    private function ensurePositiveMinutes($minutes): int
    {
        return ($minutes < 0) ? $minutes + 1440 : $minutes;
    }
}