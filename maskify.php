<?php

function maskify(string $cc): string
{
    if (strlen($cc) >= 6) {
        $maskify = [];
        $arrayString = str_split($cc);
        foreach ($arrayString as $index => $item) {
            if ($index == 0 || $index >= count($arrayString) - 4) {
                $maskify[] = $item;
                continue;
            }
            preg_match('/\D/', $item, $matches);

        if (count($matches) > 0) {
            $maskify[] = $item;
            continue;
        }
            $maskify[] = '#';
        }
    }
    else {
        return $cc;
    }

    return implode('', $maskify);
}
