<?php

class Robot
{
    public function __construct(?string $name = null) {
        if (!isset($GLOBALS['robots'])) {
            global $robots;
            $robots = [];
        }
        $this->setName();
    }

    public function setName(): string
    {
        do {
            $str_letter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $str_digit = '0123456789';
            $this->name = substr(str_shuffle($str_letter), 0,2).''.substr(str_shuffle($str_digit),0, 3);
            $name = $this->name;
        } 
        while (array_search($name, $GLOBALS["robots"]));
        $GLOBALS["robots"][] = $name;
        return $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function reset(): void
    {
        $this->setName();
    }
}
